package logUtils

import (
	"testing"
	"reflect"
)

func TestNewLogger(t *testing.T) {
	l := NewDefaultLogger()
	typeAsString := reflect.TypeOf(l).String()
	expectedOutcome := "logUtils.Logger"
	if  typeAsString != expectedOutcome {
		t.Errorf("NewDefaultLogger() did not return a Logger type, returned %s", typeAsString)
	}
}
