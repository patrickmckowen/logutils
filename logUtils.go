// logUtils saves code in defining 
// loggers in other packages.
package logUtils

import (
	"io"
	"io/ioutil"
	"log"
	"os"

	"github.com/fatih/color"
)

// Logger is custome logger type which
// currently accounts for trace, info,
// warnings, and error logs.
type Logger struct {
    Trace   *log.Logger
    Info    *log.Logger
    Warning *log.Logger
    Error   *log.Logger
}

// NewLogger takes takes 4 io.Writers for 
// trace, info, warning, and error handlers
func NewLogger(
	traceHandle io.Writer,
	infoHandle io.Writer,
	warningHandle io.Writer,
	errorHandle io.Writer)  Logger {

	var l Logger

	l.Trace = log.New(traceHandle,
		color.BlueString("TRACE: "),
		log.Ldate|log.Ltime|log.Lshortfile)

	l.Info = log.New(infoHandle,
		color.BlueString("INFO: "),
		log.Ldate|log.Ltime|log.Lshortfile)

	l.Warning = log.New(warningHandle,
		color.YellowString("WARNING: "),
		log.Ldate|log.Ltime|log.Lshortfile)

	l.Error = log.New(errorHandle,
		color.RedString("ERROR: "),
		log.Ldate|log.Ltime|log.Lshortfile)

	return l
}

// NewDefaultLogger creates a logger object 
// with sensible defaults.
func NewDefaultLogger() Logger {
	return NewLogger(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr)
}

// LoggerDemo just creates a default logger 
// and calls each of it's log types
func LoggerDemo() {
    l := NewLogger(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr)

    l.Trace.Println("I have something standard to say")
    l.Info.Println("Special Information")
    l.Warning.Println("There is something you need to know about")
    l.Error.Println("Something has failed")
}
